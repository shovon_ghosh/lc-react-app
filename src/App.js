import React, { Component } from 'react';
import logo from './logo.svg';
import Config from './config';
import Axios from 'axios';
import './App.css';

class App extends Component {
  constructor(props){
    super(props);
    this.state ={
      initialData: {}
    }
  }
  componentDidMount(){
    Axios.get(`${Config.baseUrl}${Config.entry}`).then((response)=>{
      this.setState({
        initialData: response.data
      })
    })
    .catch((error)=> {
      this.setState({
        initialData: {
          appName: Config.appName
        }
      })
    })
  }
  render() {
    return (
      <div className="App">
        <header className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <p>
            {this.state.initialData.appName}
          </p>
        </header>
      </div>
    );
  }
}

export default App;
